---
title: 19.12 RC Releases

summary: "Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

November 29, 2019. Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service.

Today they all get release candidate sources meaning they are feature complete but need testing for final bugfixes.

Distro and app store packagers should update their pre-release channels to check for issues.

+ [19.12 release notes](https://community.kde.org/Releases/19.12_Release_Notes) for information on tarballs and known issues.
+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [19.12 RC source info page](https://kde.org/info/applications-19.11.90)

## Press Contacts

For more information send us an email: [press@kde.org](mailto:press@kde.org).
