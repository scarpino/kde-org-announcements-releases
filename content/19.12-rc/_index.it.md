---
title: Rilasci RC 19.12

summary: "Sono stati rilasciati simultaneamente, come parte del servizio di
rilascio di KDE, oltre 120 singoli programmi, dozzine di librerie per
programmatori ed estensioni di funzionalità."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

29 novembre 2019. Sono stati rilasciati simultaneamente, come parte del
servizio di rilascio di KDE, oltre 120 singoli programmi, dozzine di
librerie per programmatori ed estensioni di funzionalità.

Oggi essi hanno ricevuto tutti i sorgenti release candidate, il che
significa che contengono tutte le funzionalità ma necessitano ancora di test
per le correzioni finali.

I responsabili delle distribuzioni e degli app store devono aggiornare i
loro canali di pre-rilascio per verificare eventuali problemi.

+ [note di rilascio
19.12](https://community.kde.org/Releases/19.12_Release_Notes) per
informazioni sui tarball e problemi noti.  + [Pagina wiki per lo
scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [Pagina informativa sui sorgenti RC
19.12](https://kde.org/info/applications-19.11.90)

## Contatti per la stampa

Per ulteriori informazioni scrivici un messaggio di posta elettronica::
[press@kde.org](mailto:press@kde.org).
