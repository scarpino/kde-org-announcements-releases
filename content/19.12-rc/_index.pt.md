---
title: Versões 19.12 RC

summary: "Cerca de 120 programas individuais, para além de dezenas de
bibliotecas de programação e 'plugins' de funcionalidades são lançados em
simultâneo como parte do serviço de lançamento de versões do KDE."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

29 de Novembro de 2019. Cerca de 120 programas individuais, para além de
dezenas de bibliotecas de programação e 'plugins' de funcionalidades são
lançados em simultâneo como parte do serviço de lançamento de versões do
KDE.

Hoje irão receber todos versões candidatas para lançamento, o que significa
que estão completos a nível de funcionalidades, mas precisam de testes para
correcções de erros.

Os criadores de pacotes das distribuições e lojas de aplicações deverão
actualizar os seus canais de versões prévias para procurar problemas.

+ [Notas da versão
19.12](https://community.kde.org/Releases/19.12_Release_Notes) para mais
informações sobre pacotes e problemas conhecidos.  + [Página de
transferência de pacotes da
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)  +
[Página de informação do código do 19.12
RC](https://kde.org/info/applications-19.11.90)

## Contactos de Imprensa

Para mais informações, envie-nos um e-mail:
[press@kde.org](mailto:press@kde.org).
