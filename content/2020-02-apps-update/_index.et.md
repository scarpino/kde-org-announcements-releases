---
pealkiri: KDE rakenduste uuendus veebruaris 2020

publishDate: 2020-02-06 16:00:00

layout: page # don't translate

kokkuvõte: "Mis juhtus KDE rakendustega sel kuul"

type: announcement # don't translate
---

Sel kuul on olnud hulganisti veaparandusi. Kuigi me kõik soovime aina uusi
võimalusi, teame ka seda, et kõigile meeldib, kui tüli tekitavad vead ja
krahhid ära parandatakse.

# Uued väljalasked

## KDevelop 5.5

Selle kuu suurt väljalaset pakub arenduskeskkond [KDevelop
5.5](https://www.kdevelop.org/news/kdevelop-550-released), mis lubab
hõlpsasti kirjutada programme C++, Pythoni ja PHP keeles. Me oleme
parandanud terve rea vigu ja korrastanud rohkelt KDevelopi koodi. C++ jaoks
lisasime Lambda init hõived, Clazy-tidyle ja Clangile on nüüd valik
seadistavaid valmiskontrolle ning ettevaatav lõpetamine püüab oma tööga
paremini hakkama saada. PHP sai uuendusena PHP 7.4 tüübiga omadused ning
tüübi massiivi ja klassikonstantide nähtavuse toetuse. Pythoni puhul lisati
Python 3.8 toetus.

KDevelop on saadaval sinu Linuxi distributsioonis või AppImage'ina.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) on meie kenasti Kontactiga lõimitud
ülesannete nimekirja jälgija. Kontacti viimased väljalasked olid selle
lõimituse rikkunud, nii et Zanshini kasutajatel polnud enam võimalik teada,
millised ülesanded neil veel ees seisavad. Kuid käesoleva kuu väljalaskes
[töötab see jälle](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Nüüd on
jälle hõlpus näha kõiki ülesandeid, mida meil tuleb täita!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

KDE binaarkoodi redaktori
[Okteta](https://kde.org/applications/utilities/org.kde.okteta) põhiliselt
vigade parandamisele keskendunud väljalase sisaldab ka uut võimalust: CRC-64
algoritm kontrollsumma kontrollimise tööriistale. Samuti on Okteta uuendanud
koodi vastavalt Qt ja KDE Frameworksi arendamisele.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

KDE rahaasju korraldada aitavas rakenduses [KMyMoney](https://kmymoney.org/)
[on parandatud mitmeid
vigu](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) ja
lisatud tükeldamisprotokolli tšekkide toetus

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Peale tulemas

Keysmith on Plasma Mobile ja Plasma töölaua kaheastmelise koodi
genereerija. mis kasutab oath-toolkit'i. Kasutajaliides on kirjutatud
Kirigami peale, nii et see sobitub sujuvalt ükspuha kui suurele või väiksele
ekraanile. Väljalaset on oodata peai.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) on operatsioonisüsteemi Windows
tarkvarahaldur. Chocolatey tegeleb kogu süsteemi tarkvara paigaldamise ja
uuendamisega ning võib muuta Windowsi kasutajate elu muretumaks.

[KDE-l on Chocolateys konto](https://chocolatey.org/profiles/KDE), mille
abil saab paigaldada Digikami, Krita, KDevelopi ja Kate. Paigaldamine on
imelihtne: kasutaja peab andma lihtsalt käsu 'choco install kdevelop' ja
Chocolatey hoolitseb ülejäänud eest. Chocolatey hooldajate eesmärk on
pakkuda kõrget kvaliteeti, nii et võib kindel olla, et tarkvrapaketid on
korralikult testitud ja turvalised.

Kui oled KDE rakenduse hooldaja, võiksid kaaluda võimalust lasta oma
rakendus lisada, kui aga Windowsi väljalasete uuendamise eest vastutaja,
siis just seda tarkvara oma paigalduste jaoks tarvitada.

# Veebilehekülje uuendused

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}
Meie veebimeeskond jätkab meie võrguväljundi uuendamist ja värskendas äsja [KMyMoney veebilehte](https://kmymoney.org).

# Väljalasked 19.12.2

Some of our projects release on their own timescale and some get released
en-masse. The 19.12.2 bundle of projects was released today and should be
available through app stores and distros soon. See the [19.12.2 releases
page](https://www.kde.org/info/releases-19.12.2.php) for details. This
bundle was previously called KDE Applications but has been de-branded to
become a release service to avoid confusion with all the other applications
by KDE and because there are dozens of different products rather than a
single whole.

Mõned sellekuise väljalaske parandused:

* Muusikamängija
  [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) tuleb nüüd
  toime failidega, mis ei sisalda üldse metaandmeid
* *Mustandite* kausta salvestatud kirja manused ei lähe enam kaotsi, kui
  kiri avatakse muutmiseks
  [KMailis](https://kde.org/applications/internet/org.kde.kmail2) uuesti
* Ajastamisprobleem, mis pani dokumendinäitaja
  [Okulari](https://kde.org/applications/office/org.kde.okular) renderdamist
  katkestama, sai lahenduse
* UML-i modelleerija [Umbrello](https://umbrello.kde.org/) pakub täiustatud
  Java importi

# Hankimiskohad
KDE tarkvara on saadaval paljudel platvormidel ja rakendusepoodides

[![Snapcraft](snapcraft.png)](https://snapcraft.io/publisher/kde)
[![Flathub](flathub.png)](https://flathub.org/apps/search/kde)  [![Microsoft
Store](microsoft.png)](https://www.microsoft.com/en-gb/search?q=kde)
