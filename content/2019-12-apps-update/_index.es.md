---
title: Actualización de las Aplicaciones de KDE de diciembre de 2019

summary: "¿Qué ha ocurrido este mes en las Aplicaciones de KDE?"

publishDate: 2019-12-12 13:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

# En diciembre han llegado nuevas versiones de aplicaciones de KDE

El lanzamiento de nuevas versiones de las aplicaciones de KDE es parte del
continuo esfuerzo de KDE para proporcionarle un completo y actualizado
catálogo de programas multifunción, agradables a la vista y útiles para su
sistema.

Ya están disponibles las nuevas versiones de: Dolphin, el explorador de
archivos de KDE; Kdenlive, uno de los editores de vídeo más completos de
código abierto; Okular, el visor de documentos; Gwenview, el visor de
imágenes de KDE; y el resto de sus aplicaciones y utilidades favoritas de
KDE. Todas estas aplicaciones se han mejorado para hacerlas más rápidas, más
estables y para dotarlas de interesantes funcionalidades nuevas. Las nuevas
versiones de las aplicaciones de KDE le permiten ser productivo y creativo,
a la vez que hacen que sea más fácil y más divertido el uso del software de
KDE.

Esperamos que disfrute de todas las innovadoras funcionalidades y de las
mejoras en las que hemos trabajado en todas las aplicaciones de KDE.

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) recargado

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) ha
realizado un gran lanzamiento en diciembre. La herramienta de planificación
y gestión de proyectos de KDE ha alcanzado una meta importante, ya que la
versión anterior se publicó hace casi dos años.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan le ayuda a gestionar proyectos pequeños y grandes con múltiples
recursos. Plan le ofrece diferentes tipos de dependencias entre tareas y
restricciones de tiempo para que pueda modelar su proyecto. Puede definir
tareas, estimar el esfuerzo necesario para realizar cada una de ellas,
asignar recursos y posteriormente planificar el proyecto según sus
necesidades y los recursos disponibles.

Uno de los puntos fuertes de Plan es el excelente uso de diagramas de
Gantt. Los diagramas de Gantt son gráficos compuestos de barras horizontales
que proporcionan información visual de un programa y ayudan a planificar, a
coordinar y a hacer un seguimiento de las tareas específicas de un
proyecto. El uso de diagramas de Gantt en Plan le ayudará a monitorizar
mejor el flujo de trabajo de sus proyectos.

## Suba el volumen de [Kdenlive](https://kdenlive.org)

Los desarrolladores de [Kdenlive](https://kdenlive.org) han añadido nuevas
funcionalidades y han corregido fallos a una velocidad vertiginosa. Para
esta versión han realizado más de 200 modificaciones de código.

Gran parte del trabajo se ha dedicado a mejorar el uso del sonido. En el
departamento de «fallos resueltos» se han deshecho de un problema de alto
consumo de memoria, de la ineficacia de las miniaturas de sonido y se ha
aumentado la velocidad de su almacenamiento. 

Pero todavía más interesante es que Kdenlive proporciona ahora un nuevo y
espectacular mezclador de sonido (vea la imagen). Los desarrolladores
también han añadido un nuevo visor de clips de sonido en el monitor de clips
y en el contenedor del proyecto, de modo que ahora podrá sincronizar mejor
las imágenes en movimiento con la banda sonora.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Mezclador de sonido de Kdenlive" >}}

En el departamento de «limado de asperezas», Kdenlive ha recibido muchas
mejoras de rendimiento y de usabilidad. Además, los desarrolladores han
resuelto muchos fallos en el editor de vídeo de la versión para Windows.

## Vistas previas y navegación en [Dolphin](https://userbase.kde.org/Dolphin)

[Dolphin](https://userbase.kde.org/Dolphin), el potente explorador de
archivos de KDE, incorpora nuevas funcionalidades para encontrar todo lo que
quiera buscar en el sistema de archivos. Una de ellas es la reestructuración
de las opciones de búsqueda avanzadas y otra es que ahora es posible avanzar
y retroceder en el historial de los lugares que haya visitado varias veces
manteniendo pulsado el icono de la flecha en la barra de
herramientas. También se ha revisado y corregido la función que le permite
encontrar rápidamente los archivos que haya guardado o usado recientemente.

Una de las principales preocupaciones de los desarrolladores de Dolphin es
permitir que el usuario disponga de vistas previas de los archivos antes de
abrirlos. Por ejemplo, en la nueva versión de Dolphin, puede obtener la
vista previa de un archivo GIF resaltándolo y situando a continuación el
puntero del ratón sobre el panel de vista previa. También puede reproducir
archivos de vídeo y de sonido pulsando sobre ellos en el panel de vista
previa.

Si le gustan las novelas gráficas, Dolphin puede mostrar ahora miniaturas de los libros de cómic .cb7 (consulte también el uso de este formato en Okular más abajo). Y hablando de miniaturas, la ampliación de miniaturas ha sido posible en Dolphin desde hace mucho tiempo: mantenga pulsada la tecla <kbd>Ctrl</kbd>, gire la rueda del ratón y la miniatura aumentará o se encogerá. Lo nuevo de esta función es que ahora puede restablecer la ampliación a su nivel predeterminado pulsando <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Otra útil funcionalidad de Dolphin es que ahora le mostrará los programas
que impiden que se pueda desmontar un dispositivo.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): deje que su teléfono móvil controle su escritorio

La última versión de [KDE Connect](https://community.kde.org/KDEConnect)
llega repleta de nuevas funcionalidades. Una de las más notorias es la
incorporación de una nueva aplicación de SMS que le permite leer y escribir
SMS mostrando todo el historial de la conversación.

La nueva interfaz de usuario basada en
[Kirigami](https://kde.org/products/kirigami/) significa que ahora podrá
ejecutar KDE Connect no solo en Android, sino todas las plataformas móviles
Linux que iremos viendo en futuros dispositivos, como el PinePhone y el
Librem 5. La interfaz de Kirigami también proporciona nuevas funciones para
los usuarios de escritorio a escritorio, como el control multimedia, la
entrada remota, hacer que el dispositivo suene, la transferencia de archivos
y la ejecución de órdenes.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="La nueva interfaz para el escritorio" >}}

Ya era posible usar KDE Connect para controlar el volumen de la reproducción
multimedia del escritorio, pero ahora también se puede usar para controlar
el volumen global del sistema desde el teléfono móvil (muy útil si usa el
teléfono móvil como un mando a distancia). Cuando esté dando una
conferencia, también podrá controlar la presentación usando KDE Connect para
avanzar y retroceder el pase de diapositivas.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Otra funcionalidad que ya estaba disponible desde hace algún tiempo era el
uso de KDE Connect para enviar archivos al teléfono móvil. La novedad de
esta versión es que se puede hacer que el teléfono móvil abra el archivo
tras recibirlo. KDE Itinerary usa esta funcionalidad para enviar información
sobre viajes a su teléfono móvil desde KMail.

Como nota relacionada, ahora también podrá enviar archivos desde Thunar (el
gestor de archivos de Xfce) y desde aplicaciones de Elementary, como
archivos de Pantheon. La visualización del proceso de la copia de múltiples
archivos se ha mejorado bastante y los archivos recibidos de Android
muestran ahora la hora de modificación correcta.

En cuanto a notificaciones se refiere, ahora es posible disparar acciones en
el escritorio desde notificaciones de Android, y KDE Connect usa funciones
avanzadas del centro de notificaciones de Plasma para proporcionar mejores
notificaciones. La notificación de confirmación de vinculación no dispone de
cuenta atrás con el objeto de que no se le pase inadvertida.

Finalmente, si usa KDE Connect desde la línea de órdenes, *kdeconnect-cli*
proporciona ahora terminación automática mejorada de *zsh*.

## Imágenes remotas en [Gwenview](https://userbase.kde.org/Gwenview)

[Gwenview](https://userbase.kde.org/Gwenview) le permite ahora ajustar el
nivel de compresión de JPEG al guardar las imágenes que haya editado. Los
desarrolladores también han mejorado el rendimiento de las imágenes remotas
y Gwenview ya puede importar fotos de lugares remotos y exportarlas a ellos.

## Soporte para .cb7 en [Okular](https://kde.org/applications/office/org.kde.okular)

[Okular](https://kde.org/applications/office/org.kde.okular), el visor de
documentos que le permite leer, anotar y verificar todo tipo de documentos,
incluidos PDF, EPub, ODT y MD, también permite ahora usar el formato de
libro de cómic .cb7.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): un reproductor de música sencillo y completo

[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) es un
reproductor de música que combina la sencillez con una interfaz moderna y
elegante. En esta versión se ha optimizado el aspecto de Elisa para que se
adapte mejor a las pantallas de alta densidad de píxeles. También se integra
mejor con otras aplicaciones de KDE y permite usar el sistema de menú global
de KDE.

La indexación de archivos de música también se ha mejorado y Elisa permite
ahora el uso de emisoras de radio web, de las que trae algunos ejemplos para
que pueda probar.

{{< img src="elisa.png" caption="La interfaz actualizada de Elisa" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) está preparado para pantallas táctiles

[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), el
programa predeterminado de capturas de pantalla de KDE, es otra aplicación
que ha evolucionado para adquirir capacidades táctiles: su nuevo sistema de
arrastre hace que sea mucho más fácil arrastrar rectángulos para capturar
las partes del escritorio que desee en dispositivos táctiles.

Otras mejoras son la nueva función para guardar automáticamente, que resulta
útil para realizar múltiples capturas de pantalla de forma rápida, y las
barras de avance animadas, que aseguran que pueda ver de forma clara lo que
está haciendo Spectacle.

{{<img style="max-width:500px" src="spectacle.png" caption="Nuevo sistema de arrastre de Spectacle" >}}

## Mejoras en la [Integración del navegador con Plasma](https://community.kde.org/Plasma/Browser_Integration)

La nueva versión de la [Integración del navegador con
Plasma](https://community.kde.org/Plasma/Browser_Integration) incluye ahora
una lista negra para la función de los controles multimedia. Esto resulta de
utilidad cuando se visita un sitio con gran cantidad de multimedia para
reproducir, lo que dificulta la selección de lo que se quiere controlar
desde el escritorio. La lista negra permite excluir estos sitios de los
controles multimedia.

La nueva versión también permite guardar el URL de origen en los metadatos
de los archivos y hace uso de la API de compartir la Web. Esta API permite
que los navegadores compartan enlaces, texto y archivos con otras
aplicaciones del mismo modo que las aplicaciones de KDE, mejorando la
integración de los navegadores no nativos (como Firefox, Chrome/Chromium y
Vivaldi) con el resto de aplicaciones de KDE.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Varias aplicaciones de KDE se pueden descargar de la Microsoft Store. Krita
y Okular ya llevaban algún tiempo allí, y ahora se les ha unido
[Kile](https://kde.org/applications/office/org.kde.kile), un editor de LaTeX
fácil de usar.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Novedades

Y esto nos lleva a las últimas incorporaciones:
[SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), una
aplicación que permite crear subtítulos para vídeos de una forma fácil, está
ahora en la incubadora de KDE, en camino de convertirse en miembro de pleno
derecho de la familia de aplicaciones de KDE. ¡Bienvenida!

Por otra parte, [plasma-nano](https://invent.kde.org/kde/plasma-nano), una
versión mínima de Plasma diseñada para dispositivos integrados se ha movido
a los repositorios de Plasma, lista para publicarse con 5.18.

## Lanzamiento de 19.12

Algunos de nuestros proyectos se publican según su propio calendario,
mientras que otros se publican en masa. El paquete de proyectos 19.12 se ha
publicado hoy y debería estar disponible próximamente en las tiendas de
aplicaciones y en las distribuciones. Consulte la [página de lanzamientos
19.12](https://www.kde.org/announcements/releases/19.12). Este paquete se
llamaba anteriormente Aplicaciones de KDE, pero se ha simplificado su nombre
para que se convierta en un servicio de lanzamiento, evitando así
confusiones con la totalidad de aplicaciones creadas por KDE, ya que está
compuesto de docenas de productos diferentes en lugar de su totalidad.
